package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"gitlab.com/kre1/hex/internal/api"
	"gitlab.com/kre1/hex/internal/graceful"
	"gitlab.com/kre1/hex/internal/handlers/rest"
	ilogger "gitlab.com/kre1/hex/internal/logger"
	"gitlab.com/kre1/hex/internal/repos/store"
	"gitlab.com/kre1/hex/pkg/logger"
)

func main() {
	log, err := logger.New("app", logger.UseDevelopment())
	if err != nil {
		panic(fmt.Errorf("spawning logger: %w", err))
	}

	log.Error(runApp(log), "app failed")
}

func runApp(log ilogger.Log) error {
	const (
		defaultReadTimeout     = time.Second * 3
		defaultShutdownTimeout = time.Second * 30
	)

	config := struct {
		readTimeout     time.Duration
		shutdownTimeout time.Duration
	}{
		readTimeout:     defaultReadTimeout,
		shutdownTimeout: defaultShutdownTimeout,
	} // should be read from input file

	storage, err := store.New()
	if err != nil {
		return fmt.Errorf("creating storage: %w", err)
	}

	service, err := api.New(log.Nest("api"), storage)
	if err != nil {
		return fmt.Errorf("creating api service: %w", err)
	}

	server, err := rest.New(log.Nest("rest"), service)
	if err != nil {
		return fmt.Errorf("creating restful server: %w", err)
	}

	httpServer := http.Server{Addr: ":8800", Handler: server.Handler(), ReadHeaderTimeout: config.readTimeout}

	log.Info("Starting server", "address", httpServer.Addr)

	shutdownFn := func() error {
		log.Info("Shutdown requested!")

		shCtx, done := context.WithTimeout(context.Background(), time.Second*config.shutdownTimeout)

		defer done()

		return httpServer.Shutdown(shCtx)
	}
	runFn := func() error {
		// Expecting to run forever, if shutdown initiated by call to `Shutdown()` return without error
		if lErr := httpServer.ListenAndServe(); lErr == http.ErrServerClosed {
			return nil
		}

		return err
	}

	if err := graceful.RunUntilSignal([]os.Signal{os.Interrupt, os.Kill}, runFn, shutdownFn); err != nil {
		return fmt.Errorf("app error: %w", err)
	}

	return nil
}
