package rest

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/kre1/hex/internal/api"
	"gitlab.com/kre1/hex/pkg/models"
)

func handlePortStream(reader io.Reader, service api.Service) (count int, code int, hErr error) {
	decoder := json.NewDecoder(reader)

	if err := expectDelim(decoder, '{'); err != nil {
		return 0, http.StatusBadRequest, fmt.Errorf("reading opening bracket")
	}

	for decoder.More() {
		if err := consumeString(decoder); err != nil {
			return count, http.StatusBadRequest, fmt.Errorf("reading port key: %w", err)
		}

		var data models.Port

		err := decoder.Decode(&data)
		if err != nil {
			return count, http.StatusBadRequest, fmt.Errorf("payload decode error: %w", err)
		}

		if err := service.AddPort(data); err != nil {
			return count, http.StatusInternalServerError, fmt.Errorf("adding port: %w", err)
		}

		count++
	}

	if err := expectDelim(decoder, '}'); err != nil {
		return 0, http.StatusBadRequest, fmt.Errorf("reading closing bracket: %w", err)
	}

	return count, http.StatusCreated, nil
}

func expectDelim(d *json.Decoder, delim rune) error {
	token, err := d.Token()
	if err != nil {
		return fmt.Errorf("reading json data: %w", err)
	}

	if token != json.Delim(delim) {
		return fmt.Errorf("expecting %c but got %v", delim, token)
	}

	return nil
}

func consumeString(d *json.Decoder) error {
	token, err := d.Token()
	if err != nil {
		return fmt.Errorf("reading json data: %w", err)
	}

	str, ok := token.(string)
	if !ok {
		return fmt.Errorf("expecting string token, but got (%T) %v", str, str)
	}

	return nil
}
