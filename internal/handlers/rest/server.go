package rest

import (
	"fmt"
	"net/http"
	"time"

	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/go-openapi/spec"

	"gitlab.com/kre1/hex/internal/api"
	"gitlab.com/kre1/hex/internal/logger"
	"gitlab.com/kre1/hex/pkg/models"
	"gitlab.com/kre1/hex/pkg/response"
)

func New(log logger.Log, service api.Service) (*Server, error) {
	if service == nil {
		return nil, fmt.Errorf("rest expects non-nil service")
	}

	s := &Server{
		log:       log,
		api:       service,
		container: restful.NewContainer(),
	}

	handlers := s.init()
	swagger := s.initSwaggerUI()
	openAPI := s.openAPI(handlers)

	s.container.Add(handlers)
	s.container.Add(swagger)
	s.container.Add(openAPI)

	return s, nil
}

type Server struct {
	log       logger.Log
	api       api.Service
	container *restful.Container
}

func (s *Server) Handler() http.Handler {
	return s.container
}

func (s *Server) init() *restful.WebService {
	ws := new(restful.WebService)

	ws.Route(ws.GET("ports").
		Doc("Returns all ports").
		Produces(restful.MIME_JSON).
		Returns(http.StatusOK, "OK", response.PortsResponse{}).
		Returns(http.StatusInternalServerError, "Server error", nil).
		To(s.getPorts))

	ws.Route(ws.PUT("ports").
		Doc("Accepts blob of ports").
		Produces(restful.MIME_JSON).
		Consumes(restful.MIME_JSON).
		Returns(http.StatusBadRequest, "Invalid port data", nil).
		Returns(http.StatusCreated, "Created", response.StreamPorts{}).
		To(s.streamPorts))

	ws.Route(ws.PUT("port").
		Doc("Creates or Updates single port instance").
		Consumes(restful.MIME_JSON).
		Reads(models.Port{}).
		Returns(http.StatusBadRequest, "Invalid port data", nil).
		Returns(http.StatusCreated, "Created", nil).
		To(s.putPort))

	return ws
}

func (s *Server) initSwaggerUI() *restful.WebService {
	uiPage := `<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="UTF-8">
		<title>RBAC Manager API documentation</title>
		<link rel="stylesheet" type="text/css" href="https://unpkg.com/swagger-ui-dist@3.24.0/swagger-ui.css">
	</head>
	<body>

	<div id="swagger-ui"></div>

	<script src="https://unpkg.com/swagger-ui-dist@3.24.0/swagger-ui-standalone-preset.js"></script>
	<script src="https://unpkg.com/swagger-ui-dist@3.24.0/swagger-ui-bundle.js"></script>

	<script>
		window.onload = function() {
			// Build a system
			const ui = SwaggerUIBundle({
				url: "/openapi.json",
				dom_id: '#swagger-ui',
				deepLinking: true,
				presets: [
					SwaggerUIBundle.presets.apis,
					SwaggerUIStandalonePreset
				],
				plugins: [
					SwaggerUIBundle.plugins.DownloadUrl
				],
				layout: "StandaloneLayout",
			})
			window.ui = ui
		}
	</script>
	</body>
	</html>`

	ws := new(restful.WebService)
	ws.Path("docs")
	ws.Route(ws.GET("").To(func(req *restful.Request, resp *restful.Response) {
		resp.AddHeader("content-type", "text/html")
		if _, err := resp.Write([]byte(uiPage)); err != nil {
			s.log.Error(err, "writing swagger response")
		}
	}))

	return ws
}

func (s *Server) openAPI(services ...*restful.WebService) *restful.WebService {
	return restfulspec.NewOpenAPIService(restfulspec.Config{
		WebServices: services,
		APIPath:     "/openapi.json",
		PostBuildSwaggerObjectHandler: func(swo *spec.Swagger) {
			swo.Info = &spec.Info{
				InfoProps: spec.InfoProps{
					Title:       "PortManager",
					Description: `API for managing ports database.`,
					Version:     "1",
				},
			}
		},
	})
}
func (s *Server) getPorts(req *restful.Request, resp *restful.Response) {
	startTime := time.Now()

	defer s.afterLogger(startTime, req, resp)

	ports, err := s.api.GetPorts()
	if err != nil {
		s.writeError(http.StatusInternalServerError, err, resp)
		return
	}

	if err := resp.WriteAsJson(response.PortsResponse{Ports: ports}); err != nil {
		s.log.Error(err, "writing port response")
	}
}

func (s *Server) putPort(req *restful.Request, resp *restful.Response) {
	startTime := time.Now()

	defer s.afterLogger(startTime, req, resp)

	var data models.Port

	if err := req.ReadEntity(&data); err != nil {
		s.writeError(http.StatusBadRequest, fmt.Errorf("reading request data as %T: %w", data, err), resp)
		return
	}

	err := s.api.AddPort(data)
	if err != nil {
		s.writeError(http.StatusInternalServerError, fmt.Errorf("putting port: %w", err), resp)
		return
	}

	resp.WriteHeader(http.StatusCreated)
}

func (s *Server) streamPorts(req *restful.Request, resp *restful.Response) {
	startTime := time.Now()

	defer s.afterLogger(startTime, req, resp)

	count, code, err := handlePortStream(req.Request.Body, s.api)
	if err != nil {
		s.writeError(code, err, resp)
		return
	}

	if err := resp.WriteHeaderAndJson(code, response.StreamPorts{Processed: count},
		restful.MIME_JSON); err != nil {
		s.log.Error(err, "writing port stream response")
	}
}

func (s *Server) writeError(code int, err error, resp *restful.Response) {
	if wErr := resp.WriteError(code, err); wErr != nil {
		s.log.Error(wErr, "writing response for error: %v", err)
	}
}

func (s *Server) afterLogger(start time.Time, req *restful.Request, resp *restful.Response) {
	s.log.Info(req.Request.URL.String(),
		"method", req.Request.Method,
		"status", resp.StatusCode(),
		"duration", time.Since(start))
}
