package rest

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/kre1/hex/pkg/logger"
	"gitlab.com/kre1/hex/pkg/models"
)

type mockService struct {
	calledCreate bool
	calledList   bool

	toReturnList      []models.Port
	toReturnListError error

	toReturnAdd error
}

func (ms *mockService) GetPorts() ([]models.Port, error) {
	ms.calledList = true

	return ms.toReturnList, ms.toReturnListError
}

func (ms *mockService) AddPort(models.Port) error {
	ms.calledCreate = true

	return ms.toReturnAdd
}

func TestPutPort(t *testing.T) {
	portPayload, err := json.Marshal(&models.Port{
		Name:        "hoge",
		City:        "foo",
		Country:     "Poland",
		Alias:       []string{"a", "b"},
		Regions:     []string{"x", "y"},
		Coordinates: []float64{1.1, 2.2},
		Timezone:    "CEST",
		Unlocs:      []string{"PLHGE"},
		Code:        "hge",
	})
	require.NoError(t, err)

	for _, tt := range []struct {
		name    string
		payload string

		shouldCallService bool
		serviceReturns    error

		wantStatus int
	}{{
		name:       "empty",
		wantStatus: http.StatusBadRequest,
	}, {
		name:       "wrong json",
		payload:    "hoge",
		wantStatus: http.StatusBadRequest,
	}, {
		name:              "service returns error",
		payload:           string(portPayload),
		shouldCallService: true,
		serviceReturns:    errors.New(""),
		wantStatus:        http.StatusInternalServerError,
	}, {
		name:              "OK",
		payload:           string(portPayload),
		shouldCallService: true,
		serviceReturns:    nil,
		wantStatus:        http.StatusCreated,
	}} {
		t.Run(tt.name, func(t *testing.T) {
			mock := &mockService{}

			if tt.shouldCallService {
				mock.toReturnAdd = tt.serviceReturns
			}

			server, err := New((*logger.Log)(nil), mock)
			require.NoError(t, err)

			req, err := http.NewRequest(http.MethodPut, "/port", bytes.NewReader([]byte(tt.payload)))
			require.NoError(t, err)
			req.Header.Set("Content-Type", "application/json")

			rr := httptest.NewRecorder()
			server.container.ServeHTTP(rr, req)

			if tt.shouldCallService {
				assert.True(t, mock.calledCreate, "service should be called")
			}

			assert.Equal(t, tt.wantStatus, rr.Code, "wrong status code")

			// Recorder don't need to be closed
			//nolint:bodyclose
			response, err := io.ReadAll(rr.Result().Body)
			require.NoError(t, err)

			t.Logf("response message: %s", response)
		})
	}
}

var sampleData = []byte(`
{
  "AEAJM": {
    "name": "Ajman",
    "city": "Ajman",
    "country": "United Arab Emirates",
    "alias": [],
    "regions": [],
    "coordinates": [
      55.5136433,
      25.4052165
    ],
    "province": "Ajman",
    "timezone": "Asia/Dubai",
    "unlocs": [
      "AEAJM"
    ],
    "code": "52000"
  },
  "AEAUH": {
    "name": "Abu Dhabi",
    "coordinates": [
      54.37,
      24.47
    ],
    "city": "Abu Dhabi",
    "province": "Abu Z¸aby [Abu Dhabi]",
    "country": "United Arab Emirates",
    "alias": [],
    "regions": [],
    "timezone": "Asia/Dubai",
    "unlocs": [
      "AEAUH"
    ],
    "code": "52001"
  },
  "AEDXB": {
    "name": "Dubai",
    "coordinates": [
      55.27,
      25.25
    ],
    "city": "Dubai",
    "province": "Dubayy [Dubai]",
    "country": "United Arab Emirates",
    "alias": [],
    "regions": [],
    "timezone": "Asia/Dubai",
    "unlocs": [
      "AEDXB"
    ],
    "code": "52005"
  },
  "ZWUTA": {
    "name": "Mutare",
    "city": "Mutare",
    "country": "Zimbabwe",
    "alias": [],
    "regions": [],
    "coordinates": [
      32.650351,
      -18.9757714
    ],
    "province": "Manicaland",
    "timezone": "Africa/Harare",
    "unlocs": [
      "ZWUTA"
    ]
  }
} `)

func TestStreamPorts(t *testing.T) {
	mock := &mockService{toReturnAdd: nil}
	server, err := New((*logger.Log)(nil), mock)
	require.NoError(t, err)

	for _, tt := range []struct {
		name     string
		data     []byte
		wantCode int
	}{{
		name:     "empty",
		data:     []byte("{}"),
		wantCode: http.StatusBadRequest,
	}, {
		name:     "empty port",
		data:     []byte(`{"HOGE":{}}`),
		wantCode: http.StatusCreated,
	}, {
		name:     "sampleData",
		data:     sampleData,
		wantCode: http.StatusCreated,
	}} {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodPut, "/ports", bytes.NewReader(sampleData))
			require.NoError(t, err)
			req.Header.Set("Content-Type", "application/json")

			rr := httptest.NewRecorder()
			server.container.ServeHTTP(rr, req)

			assert.True(t, mock.calledCreate, "service should be called")
			assert.Equal(t, http.StatusCreated, rr.Code, "wrong status code")
		})
	}
}

func FuzzStreamPorts(f *testing.F) {
	inputs := [][]byte{[]byte("{}"), []byte(`{"HOGE":{}}`), sampleData}

	for _, input := range inputs {
		f.Add(input)
	}

	mock := &mockService{toReturnAdd: nil}
	server, err := New((*logger.Log)(nil), mock)
	require.NoError(f, err)

	f.Fuzz(func(t *testing.T, a []byte) {
		req, err := http.NewRequest(http.MethodPut, "/ports", bytes.NewReader(sampleData))
		require.NoError(t, err)
		req.Header.Set("Content-Type", "application/json")

		rr := httptest.NewRecorder()
		server.container.ServeHTTP(rr, req)

		if rr.Code == http.StatusInternalServerError {
			// This may happen only if servise returns error, mocked service returns always no-error
			t.Fatal("Got unexpected status InternalServerError")
		}
	})
}
