package api

import (
	"gitlab.com/kre1/hex/internal/logger"
	"gitlab.com/kre1/hex/internal/repos"
	"gitlab.com/kre1/hex/pkg/models"
)

// Service manages ports
type Service interface {
	// AddPort adds a new port or update existing one
	AddPort(port models.Port) error
	GetPorts() ([]models.Port, error)
}

func New(log logger.Log, storage repos.Database) (Service, error) {
	return &service{}, nil
}

var _ Service = (*service)(nil)

type service struct {
}

func (s *service) AddPort(p models.Port) error {
	if len(p.Unlocs) == 0 {
		p.Unlocs = []string{p.Name}
	}

	return nil
}

func (s *service) GetPorts() ([]models.Port, error) {
	return nil, nil
}
