package repos

import "gitlab.com/kre1/hex/pkg/models"

type Database interface {
	InsertPort(models.Port) error
}
