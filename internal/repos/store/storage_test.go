package store

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/kre1/hex/pkg/models"
)

func TestInMemeryInsert(t *testing.T) {
	minimal1 := models.Port{
		Name:   "hoge",
		Unlocs: []string{"x"},
	}
	minimialN := models.Port{
		Name:   "hoge",
		Unlocs: []string{"a", "r", "s", "t"},
	}

	for _, tt := range []struct {
		name  string
		input models.Port

		queryKey  string
		wantEntry models.Port
	}{{
		name: "empty",
	}, {
		name:      "single unloc object",
		input:     minimal1,
		queryKey:  minimal1.Unlocs[0],
		wantEntry: minimal1,
	}, {
		name:      "multi unloc",
		input:     minimialN,
		queryKey:  minimialN.Unlocs[0],
		wantEntry: minimialN,
	}, {
		name:      "multi unloc",
		input:     minimialN,
		queryKey:  minimialN.Unlocs[3],
		wantEntry: minimialN,
	}} {
		t.Run(tt.name, func(t *testing.T) {
			db, err := New()
			require.NoError(t, err)

			assert.NoError(t, db.InsertPort(tt.input))

			assert.Equal(t, tt.wantEntry, db.data[tt.queryKey])
		})
	}
}
