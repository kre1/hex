package store

import (
	"gitlab.com/kre1/hex/pkg/models"
)

// New returns new instance of in-memory port database
func New() (*InMemory, error) {
	return &InMemory{data: map[string]models.Port{}}, nil
}

type unlocCode = string

type InMemory struct {
	data map[unlocCode]models.Port
}

// InsertPort inserts port p for every key in p.Unlocs
func (im *InMemory) InsertPort(p models.Port) error {
	for _, unloc := range p.Unlocs {
		im.data[unloc] = p
	}

	return nil
}
