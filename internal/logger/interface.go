package logger

type Log interface {
	Fatal(err error)
	Error(err error, msg string, args ...any)
	Warn(msg string, args ...any)
	Info(msg string, args ...any)
	Debug(msg string, args ...any)
	Nest(name string) Log
}
