package graceful

import (
	"context"
	"fmt"
	"os"
	"os/signal"

	"golang.org/x/sync/errgroup"
)

func RunUntilSignal(signals []os.Signal, runFn func() error, shutdownFn func() error) error {
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		wait := make(chan os.Signal, len(signals)) // chanel capacity set so sender isn't blocked

		signal.Notify(wait, signals...)

		<-wait
		cancel()
	}()

	eg, gCtx := errgroup.WithContext(ctx)

	eg.Go(runFn)
	eg.Go(func() error {
		<-gCtx.Done()
		return shutdownFn()
	})

	if err := eg.Wait(); err != nil {
		return fmt.Errorf("shutdown error: %w (%T)", err, err)
	}

	return nil
}
