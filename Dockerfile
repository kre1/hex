FROM golang:1.20.2-alpine AS build

WORKDIR /app
COPY go.* ./
RUN go mod download
ADD ./cmd ./cmd
ADD ./pkg ./pkg
ADD ./internal ./internal
RUN go build -o ./bin/app ./cmd/app

FROM scratch
WORKDIR /
COPY --from=build /app/bin/app /app
EXPOSE 8800
ENTRYPOINT ["/app"]
