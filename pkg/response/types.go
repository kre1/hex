package response

import "gitlab.com/kre1/hex/pkg/models"

type PortsResponse struct {
	Ports []models.Port `json:"ports"`
}

type PortResponse struct {
	Port models.Port `json:"port"`
}

type StreamPorts struct {
	Processed int `json:"processed"`
}
