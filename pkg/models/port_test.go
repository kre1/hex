package models

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCoordinatesMarshal(t *testing.T) {
	cases := []struct {
		name   string
		coords Coordinates
	}{{
		name: "zeros",
	}, {
		name:   "ones",
		coords: Coordinates{Latitude: 1.0, Longitude: 1.0},
	}, {
		name:   "values",
		coords: Coordinates{Latitude: 24.467, Longitude: 54.369},
	}, {
		name:   "negative",
		coords: Coordinates{Longitude: 18.4500789, Latitude: -18.1248086},
	}, {
		name:   "big longitude",
		coords: Coordinates{Longitude: 178.4500789, Latitude: -18.1248086},
	}}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			c := Coordinates{}
			data, err := json.Marshal(&tt.coords)

			assert.NoError(t, err)
			assert.NoError(t, json.Unmarshal(data, &c), "data is: %s", string(data))
			assert.Equal(t, tt.coords, c)
		})
	}
}

func TestCoordinatesUnmarshal(t *testing.T) {
	cases := []struct {
		name string
		data string

		shoulFail bool
		expected  Coordinates
	}{{
		name:      "empty",
		shoulFail: true,
	}, {
		name:      "to many numbers",
		data:      "[1.0, 2.0, 3,0]",
		shoulFail: true,
	}, {
		name:      "out of range 1",
		data:      "[1.0, 100.0]",
		shoulFail: true,
	}, {
		name:      "out of range 2",
		data:      "[1.0, -100.0]",
		shoulFail: true,
	}, {
		name:      "out of range 1",
		data:      "[200.0, 10.0]",
		shoulFail: true,
	}, {
		name:      "out of range 2",
		data:      "[-200.0, -10.0]",
		shoulFail: true,
	}}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			c := Coordinates{}

			err := json.Unmarshal([]byte(tt.data), &c)
			if tt.shoulFail {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tt.expected, c)
			}
		})
	}
}
