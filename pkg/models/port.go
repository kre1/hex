package models

import (
	"encoding/json"
	"fmt"
)

type Coordinates struct {
	Latitude  float64
	Longitude float64
}

func (c *Coordinates) UnmarshalJSON(data []byte) error {
	const valuesCount = 2

	coords := []float64{}

	if err := json.Unmarshal(data, &coords); err != nil {
		return fmt.Errorf("unmarshal Coordinates: %w", err)
	}

	if len(coords) != valuesCount {
		return fmt.Errorf("unmarshal Coordinates: wrong number of inputs: %d, should be 2", len(coords))
	}

	c.Longitude = coords[0]
	c.Latitude = coords[1]

	if c.Longitude < -180.0 || c.Longitude > 180.0 {
		return fmt.Errorf("longitude value out of range from -180 to 180: %f", c.Longitude)
	}

	if c.Latitude < -90.0 || c.Latitude > 90.0 {
		return fmt.Errorf("latitude value out of range from -90 to 90: %f", c.Latitude)
	}

	return nil
}

func (c *Coordinates) MarshalJSON() ([]byte, error) {
	coords := []float64{c.Longitude, c.Latitude}
	return json.Marshal(coords) // for slice of floats json.Marshal won't return any error - no need to wrap
}

type Port struct {
	Name        string    `json:"name"`
	City        string    `json:"city"`
	Country     string    `json:"country"`
	Alias       []string  `json:"alias"`
	Regions     []string  `json:"regions"`
	Coordinates []float64 `json:"coordinates"`
	Timezone    string    `json:"timezone"`
	Unlocs      []string  `json:"unlocs"` // UN/LOCODE - https://unece.org/trade/cefact/UNLOCODE-Download
	Code        string    `json:"code"`
}
