package logger

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/kre1/hex/internal/logger"
)

var _ logger.Log = (*Log)(nil)

const (
	NoKeyLabel = "<no-key>"
	ErrorLabel = "ERROR"

	skipFrames = 1
)

// New creates new logger using zap as actual logger
func New(instanceName string, opts ...OptFunc) (*Log, error) {
	params := Opts{}

	for _, optFunc := range opts {
		optFunc(&params)
	}

	var cfg zap.Config
	if params.development {
		cfg = zap.NewDevelopmentConfig()
		cfg.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	} else {
		cfg = zap.NewProductionConfig()
	}

	zapOptions := []zap.Option{zap.AddCallerSkip(skipFrames)}

	if len(params.raw) > 0 {
		zapOptions = append(zapOptions, params.raw...)
	}

	cfg.Sampling = nil

	return buildLogger(instanceName, cfg, zapOptions)
}

type Log struct {
	z *zap.SugaredLogger
}

// Fatal is intended to be called only from `main` function, it calls os.Exit for non-nil err
func (l *Log) Fatal(err error) {
	if l == nil || err == nil {
		return
	}

	l.z.Fatal(err)
}

// Error logs message with error level, if debug is enabled it prints stack trace
func (l *Log) Error(err error, msg string, args ...any) {
	if l == nil || err == nil {
		return
	}

	l.z.Errorw(msg, append(pairArgs(args...), "ERROR", err.Error())...)
}

// Warn logs message with error level, if debug is enabled it prints stack trace
func (l *Log) Warn(msg string, args ...any) {
	if l == nil {
		return
	}

	l.z.Warnw(msg, pairArgs(args...)...)
}

// Info logs message with info level
func (l *Log) Info(msg string, args ...any) {
	if l == nil {
		return
	}

	l.z.Infow(msg, pairArgs(args...)...)
}

// Debug logs message only if debug (`UseDevelopment()`) was enabled
func (l *Log) Debug(msg string, args ...any) {
	if l == nil {
		return
	}

	l.z.Debugw(msg, pairArgs(args...)...)
}

// Nest returns new logger with resulting name as joined name from base logger with new name
func (l *Log) Nest(name string) logger.Log {
	if l == nil {
		return nil
	}

	return &Log{z: l.z.Named(name)}
}

func pairArgs(args ...any) []any {
	const pair = 2
	if (len(args) % pair) != 0 {
		args = append(args, args[len(args)-1])
		args[len(args)-2] = NoKeyLabel
	}

	return args
}

type Opts struct {
	raw         []zap.Option
	development bool
}

type OptFunc func(*Opts)

// UseDevelopment enables logging of debug messages and prints stack trace for Warn and Error levels
func UseDevelopment() OptFunc {
	return func(opts *Opts) {
		opts.development = true
	}
}

// WithZapOption passer raw zap logger option
func WithZapOption(o zap.Option) OptFunc {
	return func(opts *Opts) {
		opts.raw = append(opts.raw, o)
	}
}

func buildLogger(name string, cfg zap.Config, opts []zap.Option) (*Log, error) {
	l, err := cfg.Build(opts...)
	if err != nil {
		return nil, fmt.Errorf("making zap logger %s with config: %+v and options: %v failed with error: %w",
			name, cfg, opts, err)
	}

	return &Log{
		z: l.Named(name).Sugar(),
	}, nil
}
