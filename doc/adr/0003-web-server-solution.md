# 3. WEB Server solution

Date: 2023-07-16

## Status

Accepted

## Context

Service should run with web server handling incoming requests.
Since this should show how hex architecture might be used with go only simple http servers are considered.

It is always nice to have some input about what service is capable of handling. Therefore automatic swagger/openAPI
generation would be useful.

## Decision

One of the simple solutions is https://github.com/emicklei/go-restful which uses plain go http.Server
and allow to set handlers in a very similar way.

## Consequences

Auto-generation and serving of swagger is very easy to use.
