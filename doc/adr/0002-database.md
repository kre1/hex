# 2. Database

Date: 2023-07-16

## Status

Accepted

## Context

Application needs to store data to work correctly.
Currently only Port data need to be stored.

Since this is just a simple example of hex architecture may be implemented with Go,
permanent storage is not really needed.
Example for non-permanent storage would be to use in-memory map to store elements indexed by key.

## Decision

For simplicity in-memory storage will be used.
Stored elements (Ports) will be indexed by Unlocs (UN/LOCODE) strings since this identify the Port unambiguously.

## Consequences

Obviously after application restart any data will be lost.
